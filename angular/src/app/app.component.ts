
import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { UsersService } from './users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit
{
  title = 'sicAngular';
  displayedColumns: string[] = ['id','email',  'name', 'gender', 'status'];
  displayedColumns2: string[] = ['nombre','apellido',   'email'];
  dataSource = new MatTableDataSource<PeriodicElement>([]);
  dataSourceEncuestas = new MatTableDataSource<PeriodicElement>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
  }
  constructor(private userSvc: UsersService){
    this.userSvc.getUsers().subscribe(users =>
      {
        console.log(users)
        let data = users;
        this.dataSource = new MatTableDataSource(data.data);
    this.dataSource.paginator = this.paginator;

      });

      this.userSvc.getEncuestas().subscribe(users =>
        {
          console.log(users)
          let data = users;
          this.dataSourceEncuestas = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
  
        });
  }
}

export interface PeriodicElement {
  email: string;
  id: number;
  name: string;
  gender: string;
  status: string
}


