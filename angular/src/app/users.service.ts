import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) { }

  getUsers() :Observable<any> {
    return this.http.get("https://gorest.co.in/public/v1/users");
  }

  getEncuestas(): Observable<any>{
    return this.http.get("http://localhost:8080/encuestas/all");
    
  }
}