package com.example.demo.entidadesForm;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
public class EncuestaForm {

	private long id;
	@NotNull(message = "Campo requerido")
	@Size(min=2, max=30, message="Debe tener entre 2 y 100 carácteres")
	private String nombre;
	
	@NotNull(message = "Campo requerido")
	@Size(min=2, max=30, message="Debe tener entre 2 y 100 carácteres")
	private String apellido;
	
	@Email(message = "El correo electrónico debe ser válido")
	private String email;
	
	@NotNull(message = "Campo requerido")
	private Integer marca;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getMarca() {
		return marca;
	}

	public void setMarca(Integer marca) {
		this.marca = marca;
	}

}