package com.example.demo.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.example.demo.PersonForm;
import com.example.demo.entidades.Encuesta;
import com.example.demo.entidadesForm.EncuestaForm;
import com.example.demo.repositorio.EncuestaRepositorio;
import com.example.demo.repositorio.MarcaRepositorio;


@Controller
public class WebController implements WebMvcConfigurer {
	@Autowired 
	private MarcaRepositorio repositorioMarca;
	
	@Autowired 
	private EncuestaRepositorio repositorioEncuesta;
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/results").setViewName("results");
	}

	@GetMapping("/")
	public String showForm(EncuestaForm encuestaForm, Model model) {
		 var marcas= repositorioMarca.findAll();
		 model.addAttribute("marcas",marcas);
		return "form";
	}

	@PostMapping("/")
	public String checkPersonInfo(@Valid EncuestaForm encuestaForm, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return "form";
		} else {
			Encuesta e = new Encuesta();
			e.setApellido(encuestaForm.getApellido());
			e.setEmail(encuestaForm.getEmail());
			e.setMarca(encuestaForm.getMarca());
			e.setNombre(encuestaForm.getNombre());
			repositorioEncuesta.save(e);
		}

		return "redirect:/results";
	}
}