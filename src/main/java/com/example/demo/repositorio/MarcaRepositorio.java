package com.example.demo.repositorio;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.entidades.MarcaPc;


public interface MarcaRepositorio extends CrudRepository<MarcaPc, Integer> {

}