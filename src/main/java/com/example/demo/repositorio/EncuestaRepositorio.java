package com.example.demo.repositorio;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.entidades.Encuesta;


public interface EncuestaRepositorio extends CrudRepository<Encuesta, Integer> {

}

