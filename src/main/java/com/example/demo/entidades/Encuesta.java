package com.example.demo.entidades;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class Encuesta {
	@Id
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private Integer id;

	  private String apellido;
	  private String email;
	  private Integer marcaPC;
	  private String nombre;
	  
	  public void setId(Integer id) {
		    this.id = id;
		  }
	  public Integer getId() {
		    return id;
		  }
	  
	 public void setApellido(String apellido){
		 this.apellido = apellido;
	 }
	 
	 public String getApellido() {
		 return this.apellido;
	 }
	 
	 public void setMarca(Integer marca) {
		 this.marcaPC = marca;
	 }
	 public Integer getMarca() {
		 return this.marcaPC;
	 }
	 public void setNombre(String nombre) {
		 this.nombre = nombre;
	 }
	 
	 public String getNombre(){
		 return this.nombre;
	 }
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
