package com.example.demo.entidades;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class MarcaPc {
	@Id
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private Integer id;

	  private String marca;
	  
	  public void setId(Integer id) {
		    this.id = id;
		  }
	  public Integer getId() {
		    return id;
		  }
	  
	  public String getMarca() {
		    return marca;
		  }

		  public void setMarca(String marca) {
		    this.marca = marca;
		  }
}
